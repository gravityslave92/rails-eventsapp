// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require moment
//= require moment/ru.js
//= require tempusdominus-bootstrap-4.js
//= require cable
//= require_tree .

'use strict';

$(document).ready(function () {
    var minDateVal =  $('#event_date_start_min_value').val();
    var maxDateVal =  $('#event_date_start_max_value').val();

    if (!!minDateVal) {
        $('[data-target="#event_date_start_min"]').val(minDateVal)
    }

    if (!!maxDateVal) {
        $('[data-target="#event_date_start_max"]').val(maxDateVal)
    }

    $('#event_date_start_min').datetimepicker({locale: 'ru'});
    $('#event_date_start_max').datetimepicker({locale: 'ru'});
    $('#event_form').submit( function (e) {
      e.preventDefault();
      var body = {
        city: $('#city').val(),
        theme_list: $('#theme_list').val(),
        date_start_min: $('[data-target="#event_date_start_min"]').val(),
        date_start_max: $('[data-target="#event_date_start_max"]').val(),
        user_id: $('#current_user_id').val()
      };

      if (!!$('#save_preferences').prop('checked')) {
        body['save'] = 'on';
      }

      $.ajax({
        url: '/events/search',
        type: 'POST',
        data: {request: JSON.stringify(body)},
        success: function (res) {
          var parent = $('#result_row');
          parent.empty();
          res.forEach(function (el) {
            var template = `
              <div class="col-8 my-2">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title"> Проводится в ${el['city']} </h5>
                    <h6 class="card-subtitle mb-2 text-muted"> По адресу ${el['place'] || 'в городе'}</h6>
                    <p class="card-text"> Время и дата начала: ${el['date_start']}</p>                       
                  </div>
                </div>
              </div>`;
            parent.append(template);
          })
      },
      error: function () {
        console.error('error')
      }
    });
  })
});

