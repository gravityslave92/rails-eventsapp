class AppearanceChannel < ApplicationCable::Channel
=begin
  def subscribed
    current_user.online!
  end

  def unsubscribed
    current_user.offline!
  end
=end
end
