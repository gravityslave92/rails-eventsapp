class EventsController < ApplicationController
  before_action :get_event, only: %i[show edit destroy update]
  before_action :authenticate_user!, only: %i[new edit :destroy]
  def index
   @events = Event.all
  end

  def show; end

  def new
    @event = Event.new
    @event.creator = current_user
  end

  def create
    @event = Event.new event_params.to_hash.except('theme_list')
    @event.theme_list.add(event_params['theme_list'], parse: true)
    if @event.save
      redirect_to event_path(@event.id),
                  notice: %(Cобытие #{@event.title} успешно создано)
    else
      redirect_to  new_event_path, alert: 'Произошла ошибка!'
    end
  end

  def edit; end

  def update
    @event.update event_params.to_hast.except('theme_list')
  end

  def destroy
    title = @event.title
    @event.delete
    redirect_to events_path, alert: %(Событие #{title} отменено)
  end

  def event_search
    if current_user
      redis = Redis.new
      if redis.exists %(user:preferences:#{current_user.id})
        @setting = redis.hgetall( %(user:preferences:#{current_user.id})).to_hash
      end
    end
  end

  def post_event_search
    body = JSON.parse params['request']
    ret = []
    if 'on' == body['save']
      settings = body.except('save', 'user_id').to_a.flatten
      Redis.new.hmset %(user:preferences:#{body['user_id']}), settings
    end
    if body['theme_list'].present?
      tags = body['theme_list'].split(',').map { |el| %("#{el}") }
      ret << %(by_tags(#{tags}))
    end
    ret << %(by_city("#{body['city']}")) if body['city'].present?
    if body['date_start_min'].present? && body['date_start_max'].present?
      ret << %(by_start("#{body['date_start_min']}","#{body['date_start_max']}"))
    end
    render json: Event.class_eval(ret.join('.'))
  end

  private


  def get_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(
      :id, :date_start, :date_end, :place,
      :city, :title, :creator_id, :theme_list
    )
  end
end
