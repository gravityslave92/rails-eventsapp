module ApplicationHelper

  def bootstrap_class_for(flash_type)
    {
      success: 'alert-success',
      error: 'alert-danger',
      alert: 'alert-warning',
      notice: 'alert-primary'
    }[flash_type.to_sym] || flash_type.to_s
  end

  def get_value_safe(hsh, key)
    begin
      return hsh.fetch key, ''
    rescue NameError
      return ''
    end
  end
end
