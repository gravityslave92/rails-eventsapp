class NoticeMailer < ApplicationMailer
  def notice_user(uid, event_id)
    user = User.find(uid)
    event = Event.find(event_id)
    mail to: user.email,
         subject: %(#{event.title} ждет вас!),
         body:  %(в #{event.place || 'город'} приходите в #{event.date_start})
  end
end
