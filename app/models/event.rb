class Event < ApplicationRecord
  acts_as_taggable_on :themes
  acts_as_followable
  acts_as_likeable

  after_commit lambda {
    ::SnoopWorker.perform_async id
  }, on: :create

  has_and_belongs_to_many :users
  belongs_to :creator, class_name: 'User', foreign_key: :creator_id, required: true
  validates_presence_of :title, :city, :date_start, :date_end


  scope :by_tags, ->(tag_list)  { tagged_with(tag_list, any: true) }
  scope :by_start, ->(start, end_date) { where(date_start: start.to_datetime..end_date.to_datetime) }
  scope :by_city, ->(city) { where(city: city) }


end
