class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :events, class_name: 'Event'
  has_many :created_events, class_name: 'Event', foreign_key: :creator_id
  acts_as_taggable_on :interests
  acts_as_follower
  acts_as_liker

  def offline!
    redis.setbit('user_appearance', id, 0)
  end

  def offline?
    redis.getbit('user_appearance', id).zero?
  end

  def online
    redis.setbit('user_appearance', id, 0)
  end

  def online?
    redis.getbit('user_appearance', id) == 1
  end


  private

  def redis
    Redis.new
  end
end
