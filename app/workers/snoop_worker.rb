class SnoopWorker
  include Sidekiq::Worker

  def perform(event_id)
    event = Event.find(event_id)
    redis = Redis.new

    User.pluck(:id).each do |uid|
      next unless redis.exists(%(user:preferences:#{uid}))
      attrs = redis.hgetall %(user:preferences:#{uid})
      themes_arr = attrs['theme_list'].split(',')
      if event.city == attrs['city'] ||
         intersect?(event.theme_list, themes_arr) ||
         in_range?(
           date: event.date_start,
           range_min: attrs['date_start_min'],
           range_max: attrs['date_start_max']
         )
        if User.find(uid).offline?
          NoticeMailer.notice_user(uid, event_id)
        else
          next # @todo
        end
      else
        next
      end
    end
  end

  private


  def intersect?(arr1, arr2)
    return false if !arr1.present? || !arr2.present?
    !(arr1 & arr2).empty?
  end

  def in_range?(date:, range_min:, range_max:)
    range_min.to_datetime < date && date < range_max.to_datetime
  end
end
