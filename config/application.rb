require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CityOfEvents
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.log_level = :error
    config.active_job.queue_adapter = :sidekiq
    config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
    config.autoload_paths += %W(#{Rails.root}/app/workers)
    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru
    config.sass.preferred_syntax = :sass
    config.action_cable.mount_path = '/cable'
    config.action_cable.disable_request_forgery_protection = true
    config.cache_store = :redis_store, {
        host: 'localhost',
        port: 6379,
        db: 0,
        namespace: 'frontend_cache'
    }, {
        expires_in: 1.day
    }


    # Don't generate system test files.
    config.generators do |g|
      g.skip_routes true
      g.assets false
      g.helper false
    end
  end
end
