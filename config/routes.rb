Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  root to: 'home#index'

  resources :events do
    collection do
      get 'search', to: 'events#event_search', as: :search
      post 'search', to: 'events#post_event_search'
    end
  end

  mount ActionCable.server => '/cable'
end
