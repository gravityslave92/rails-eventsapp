class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.datetime :date_start
      t.datetime :date_end
      t.string :place
      t.string :city, null: false
      t.string :title, null: false

      t.timestamps
    end
  end
end
