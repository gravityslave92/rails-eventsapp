class UsersEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events_users, id: false do |t|
      t.references :user, index: true
      t.references :event, index: true
    end
  end
end
