# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.first

10.times do |indx|
  event = Event.new(
    title: Faker::BojackHorseman.character,
    city: Faker::Address.city,
    place: Faker::Address.street_address,
    date_start:  DateTime.now - indx.day,
    date_end:  DateTime.now + indx.day,
    creator_id: user.id
  )
  event.theme_list = [ Faker::Dota.hero, Faker::Dota.player ].join(',')
  event.save!
end