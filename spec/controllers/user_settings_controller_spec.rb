require 'rails_helper'

RSpec.describe UserSettingsController, type: :controller do

  describe "GET #save_settings" do
    it "returns http success" do
      get :save_settings
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #get_settings" do
    it "returns http success" do
      get :get_settings
      expect(response).to have_http_status(:success)
    end
  end

end
